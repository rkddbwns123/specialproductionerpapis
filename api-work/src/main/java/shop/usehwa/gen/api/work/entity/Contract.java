package shop.usehwa.gen.api.work.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.tomcat.jni.Local;
import shop.usehwa.gen.api.work.model.ContractCreateRequest;
import shop.usehwa.gen.api.work.model.ContractPhoneUpdateRequest;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Contract {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "패키지 관리 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "pacakgeId")
    private PackageInfo packageInfo;

    @ApiModelProperty(notes = "공정 관리 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "presentProcessId")
    private Process presentProcess;

    @ApiModelProperty(notes = "계약 번호")
    @Column(nullable = false, length = 20, unique = true)
    private String contractNumber;

    @ApiModelProperty(notes = "고객명")
    @Column(nullable = false, length = 20)
    private String customerName;

    @ApiModelProperty(notes = "고객 전화번호")
    @Column(nullable = false, length = 13, unique = true)
    private String customerPhone;

    @ApiModelProperty(notes = "계약일자")
    @Column(nullable = false)
    private LocalDate contractDate;

    @ApiModelProperty(notes = "업무 시작일")
    @Column(nullable = false)
    private LocalDate startWorkDate;

    @ApiModelProperty(notes = "업무 종료일")
    private LocalDate endWorkDate;

    public void putCustomerPhone(ContractPhoneUpdateRequest request) {
        this.customerPhone = request.getPhoneNumber();
    }
    public void finishWork(Process process) {
        this.presentProcess = process;
    }
    public void putEndWorkDate() {
        this.endWorkDate = LocalDate.now();
    }
    private Contract(ContractBuilder builder) {
        this.packageInfo = builder.packageInfo;
        this.presentProcess = builder.presentProcess;
        this.contractNumber = builder.contractNumber;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.contractDate = builder.contractDate;
        this.startWorkDate = builder.startWorkDate;
        this.endWorkDate = builder.endWorkDate;

    }

    public static class ContractBuilder implements CommonModelBuilder<Contract> {

        private final PackageInfo packageInfo;
        private final Process presentProcess;
        private final String contractNumber;
        private final String customerName;
        private final String customerPhone;
        private final LocalDate contractDate;
        private final LocalDate startWorkDate;
        private final LocalDate endWorkDate;

        public ContractBuilder(ContractCreateRequest request, PackageInfo packageInfo, Process process) {
            this.packageInfo = packageInfo;
            this.presentProcess = process;
            this.contractNumber = request.getContractNumber();
            this.customerName = request.getCustomerName();
            this.customerPhone = request.getCustomerPhone();
            this.contractDate = request.getContractDate();
            this.startWorkDate = LocalDate.now();
            this.endWorkDate = null;
        }

        @Override
        public Contract build() {
            return new Contract(this);
        }
    }
}
