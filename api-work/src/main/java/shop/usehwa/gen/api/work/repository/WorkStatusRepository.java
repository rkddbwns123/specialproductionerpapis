package shop.usehwa.gen.api.work.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.work.entity.Contract;
import shop.usehwa.gen.api.work.entity.WorkStatus;

import java.awt.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface WorkStatusRepository extends JpaRepository<WorkStatus, Long> {
    List<WorkStatus> findByTeam_Id(long teamId);

    List<WorkStatus> findByApprover(String approver);

    List<WorkStatus> findByCreateDateBetween(LocalDateTime startDate, LocalDateTime endDate);

    List<WorkStatus> findAllByContract_ContractNumberAndContract_CustomerPhone(String contractNumber, String customerPhone);


    List<WorkStatus> findAllByContract(Contract contract);
}
