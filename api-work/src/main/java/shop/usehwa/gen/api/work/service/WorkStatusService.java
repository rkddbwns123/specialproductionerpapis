package shop.usehwa.gen.api.work.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.work.entity.WorkRecord;
import shop.usehwa.gen.api.work.entity.WorkStatus;
import shop.usehwa.gen.api.work.model.WorkRecordItem;
import shop.usehwa.gen.api.work.model.WorkStatusItem;
import shop.usehwa.gen.api.work.model.WorkStatusMiniResponse;
import shop.usehwa.gen.api.work.model.WorkStatusResponse;
import shop.usehwa.gen.api.work.repository.WorkRecordRepository;
import shop.usehwa.gen.api.work.repository.WorkStatusRepository;
import shop.usehwa.gen.common.exception.CDontFindWorkStatusException;
import shop.usehwa.gen.common.exception.CNotRegistrationWorkStatusException;
import shop.usehwa.gen.common.response.model.ListResult;
import shop.usehwa.gen.common.response.service.ListConvertService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WorkStatusService {
    private final WorkStatusRepository workStatusRepository;

    private final WorkRecordRepository workRecordRepository;

    public ListResult<WorkStatusItem> getList() {
        List<WorkStatus> originList = workStatusRepository.findAll();

        List<WorkStatusItem> result = new LinkedList<>();

        if (originList.isEmpty()) throw new CNotRegistrationWorkStatusException();
        originList.forEach(item -> result.add(new WorkStatusItem.WorkStatusItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<WorkStatusItem> getListByTeam(long teamId) {
        List<WorkStatus> originList = workStatusRepository.findByTeam_Id(teamId);

        List<WorkStatusItem> result = new LinkedList<>();

        if (originList.isEmpty()) throw new CDontFindWorkStatusException();
        originList.forEach(item -> result.add(new WorkStatusItem.WorkStatusItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<WorkStatusItem> getListByApprover(String approver) {
        List<WorkStatus> originList = workStatusRepository.findByApprover(approver);

        List<WorkStatusItem> result = new LinkedList<>();

        if (originList.isEmpty()) throw new CDontFindWorkStatusException();
        originList.forEach(item -> result.add(new WorkStatusItem.WorkStatusItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<WorkStatusItem> getListByDateBetween(LocalDate startDate, LocalDate endDate) {
        LocalDateTime startDateTime = startDate.atTime(0,0,0);
        LocalDateTime endDateTime = endDate.atTime(23,59,59);

        List<WorkStatus> originList = workStatusRepository.findByCreateDateBetween(startDateTime, endDateTime);

        List<WorkStatusItem> result = new LinkedList<>();

        if (originList.isEmpty()) throw new CDontFindWorkStatusException();
        originList.forEach(item -> result.add(new WorkStatusItem.WorkStatusItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public WorkStatusResponse getWorkStatus(long workStatusId) {
        WorkStatus workStatus = workStatusRepository.findById(workStatusId).orElseThrow(CDontFindWorkStatusException::new);

        return new WorkStatusResponse.WorkStatusResponseBuilder(workStatus).build();
    }

    public WorkStatusMiniResponse getWorkStatusMini(long workStatusId) {
        WorkStatus workStatus = workStatusRepository.findById(workStatusId).orElseThrow(CDontFindWorkStatusException::new);

        List<WorkRecord> workRecords = workRecordRepository.findByContract_IdAndProcess_Id(
                workStatus.getContract().getId(), workStatus.getContract().getPresentProcess().getId());

        List<WorkRecordItem> workRecordItems = new LinkedList<>();

        workRecords.forEach(item -> workRecordItems.add(new WorkRecordItem.WorkRecordItemBuilder(item).build()));

        return new WorkStatusMiniResponse.WorkStatusMiniResponseBuilder(workStatus, workRecordItems).build();
    }
}
