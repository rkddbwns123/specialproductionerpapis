package shop.usehwa.gen.api.work.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.work.entity.WorkStatus;
import shop.usehwa.gen.common.function.ConvertFormat;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CustomerViewItem {
    private String processName;

    private String createDate;

    private String finishDate;

    private String mainRecordImg;

    private CustomerViewItem(CustomerViewItemBuilder builder) {
        this.processName = builder.processName;
        this.createDate = builder.createDate;
        this.finishDate = builder.finishDate;
        this.mainRecordImg = builder.mainRecordImg;
    }

    public static class CustomerViewItemBuilder implements CommonModelBuilder<CustomerViewItem> {

        private final String processName;
        private final String createDate;
        private final String finishDate;
        private final String mainRecordImg;

        public CustomerViewItemBuilder(WorkStatus workStatus) {
            this.processName = workStatus.getProcess().getProcessName();
            this.createDate = ConvertFormat.convertLocalDateTimeToStringMini(workStatus.getCreateDate());
            if (workStatus.getFinishDate() == null) {
                this.finishDate = "";
            } else {
                this.finishDate = ConvertFormat.convertLocalDateTimeToStringMini(workStatus.getFinishDate());
            }
            this.mainRecordImg = workStatus.getWorkRecord().getImageAddress();
        }

        @Override
        public CustomerViewItem build() {
            return new CustomerViewItem(this);
        }
    }
}
