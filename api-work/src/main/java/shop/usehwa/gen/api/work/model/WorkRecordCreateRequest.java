package shop.usehwa.gen.api.work.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import shop.usehwa.gen.api.work.entity.Contract;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class WorkRecordCreateRequest {

    @ApiModelProperty(notes = "사진 이미지 주소")
    @NotNull
    @Length(min = 1)
    private String imageAddress;

    @ApiModelProperty(notes = "메모")
    @NotNull
    private String memo;

    @ApiModelProperty(notes = "작업자명")
    @NotNull
    @Length(min = 2, max = 20)
    private String workerName;
}
