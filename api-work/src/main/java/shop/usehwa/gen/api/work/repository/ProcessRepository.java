package shop.usehwa.gen.api.work.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.work.entity.PackageInfo;
import shop.usehwa.gen.api.work.entity.Process;

import java.util.List;

public interface ProcessRepository extends JpaRepository<Process, Long> {
    List<Process> findByPackageInfoOrderByProcessOrderAsc(PackageInfo packageInfo);
}
