package shop.usehwa.gen.api.work.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.work.entity.Contract;
import shop.usehwa.gen.common.function.ConvertFormat;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ContractResponse {
    private String packageName;

    private String presentProcess;

    private String contractNumber;

    private String customerName;

    private String customerPhone;

    private String contractDate;

    private String startWorkDate;

    private String teamName;

    private ContractResponse(ContractResponseBuilder builder) {
        this.packageName = builder.packageName;
        this.presentProcess = builder.presentProcess;
        this.contractNumber = builder.contractNumber;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.contractDate = builder.contractDate;
        this.startWorkDate = builder.startWorkDate;
        this.teamName = builder.teamName;
    }

    public static class ContractResponseBuilder implements CommonModelBuilder<ContractResponse> {

        private final String packageName;
        private final String presentProcess;
        private final String contractNumber;
        private final String customerName;
        private final String customerPhone;
        private final String contractDate;
        private final String startWorkDate;
        private final String teamName;

        public ContractResponseBuilder(Contract contract) {
            this.packageName = contract.getPackageInfo().getPackageName();
            this.presentProcess = contract.getPresentProcess().getProcessName();
            this.contractNumber = contract.getContractNumber();
            this.customerName = contract.getCustomerName();
            this.customerPhone = contract.getCustomerPhone();
            this.contractDate = ConvertFormat.convertLocalDateToString(contract.getContractDate());
            this.startWorkDate = ConvertFormat.convertLocalDateToString(contract.getStartWorkDate());
            this.teamName = contract.getPresentProcess().getTeam().getTeamName();
        }

        @Override
        public ContractResponse build() {
            return new ContractResponse(this);
        }
    }
}
