package shop.usehwa.gen.api.work.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class WorkNextProcessRequest {
    @ApiModelProperty(notes = "승인자")
    @NotNull
    @Length(min = 2, max = 20)
    private String approver;
}
