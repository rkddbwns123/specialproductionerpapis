package shop.usehwa.gen.api.work.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.work.entity.Contract;
import shop.usehwa.gen.api.work.entity.Process;
import shop.usehwa.gen.api.work.entity.WorkStatus;
import shop.usehwa.gen.api.work.model.CustomerViewItem;
import shop.usehwa.gen.api.work.model.CustomerViewResponse;
import shop.usehwa.gen.api.work.model.WorkStatusItem;
import shop.usehwa.gen.api.work.repository.ContractRepository;
import shop.usehwa.gen.api.work.repository.ProcessRepository;
import shop.usehwa.gen.api.work.repository.WorkStatusRepository;
import shop.usehwa.gen.common.exception.CDontFindProcessInfoException;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerViewService {
    private final WorkStatusRepository workStatusRepository;
    private final ProcessRepository processRepository;
    private final ContractRepository contractRepository;

    public CustomerViewResponse getCustomerView(String contractNumber, String customerPhone) {
        List<WorkStatus> workStatusList = workStatusRepository.findAllByContract_ContractNumberAndContract_CustomerPhone(contractNumber, customerPhone);

        List<CustomerViewItem> result = new LinkedList<>();

        workStatusList.forEach(item -> result.add(new CustomerViewItem.CustomerViewItemBuilder(item).build()));

        Contract contract = contractRepository.findByContractNumber(contractNumber);

        List<Process> processList = processRepository.findByPackageInfoOrderByProcessOrderAsc(contract.getPackageInfo());

        int processCnt = processList.size();

        String currentProgress = contract.getPresentProcess().getProcessName() + (contract.getPresentProcess().getProcessOrder() / processCnt) * 100 + "%";

        return new CustomerViewResponse.CustomerViewResponseBuilder(contract, currentProgress, result).build();
    }
}
