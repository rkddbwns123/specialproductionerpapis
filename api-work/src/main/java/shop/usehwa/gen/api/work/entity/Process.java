package shop.usehwa.gen.api.work.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Process {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "팀 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "teamId")
    private Team team;

    @ApiModelProperty(notes = "공정 이름")
    @Column(nullable = false, length = 50)
    private String processName;

    @ApiModelProperty(notes = "공정 순서")
    @Column(nullable = false)
    private Integer processOrder;

    @ApiModelProperty(notes = "패키지 관리 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "packageInfoId")
    private PackageInfo packageInfo;

    @ApiModelProperty(notes = "사용 중 여부")
    @Column(nullable = false)
    private Boolean isUse;

    @ApiModelProperty(notes = "등록일자")
    @Column(nullable = false)
    private LocalDateTime createDate;

    @ApiModelProperty(notes = "메모")
    @Column(columnDefinition = "TEXT")
    private String memo;
}
