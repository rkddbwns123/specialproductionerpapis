package shop.usehwa.gen.api.work.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.work.entity.WorkStatus;
import shop.usehwa.gen.common.function.ConvertFormat;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WorkStatusMiniResponse {
    private String createDate;

    private String contractNumber;

    private String processName;

    private String teamName;

    private Long contractId;

    private List<WorkRecordItem> workRecordItems;

    private WorkStatusMiniResponse(WorkStatusMiniResponseBuilder builder) {
        this.createDate = builder.createDate;
        this.contractNumber = builder.contractNumber;
        this.processName = builder.processName;
        this.teamName = builder.teamName;
        this.contractId = builder.contractId;
        this.workRecordItems = builder.workRecordItems;
    }

    public static class WorkStatusMiniResponseBuilder implements CommonModelBuilder<WorkStatusMiniResponse> {

        private final String createDate;
        private final String contractNumber;
        private final String processName;
        private final String teamName;
        private final Long contractId;
        private final List<WorkRecordItem> workRecordItems;

        public WorkStatusMiniResponseBuilder(WorkStatus workStatus, List<WorkRecordItem> workRecordItems) {
            this.createDate = ConvertFormat.convertLocalDateTimeToStringMini(workStatus.getCreateDate());
            this.contractNumber = workStatus.getContract().getContractNumber();
            this.processName = workStatus.getProcess().getProcessName();
            this.teamName = workStatus.getTeam().getTeamName();
            this.contractId = workStatus.getContract().getId();
            this.workRecordItems = workRecordItems;
        }

        @Override
        public WorkStatusMiniResponse build() {
            return new WorkStatusMiniResponse(this);
        }
    }
}
