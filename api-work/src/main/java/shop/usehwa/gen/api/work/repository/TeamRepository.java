package shop.usehwa.gen.api.work.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.work.entity.Team;

public interface TeamRepository extends JpaRepository<Team, Long> {
}
