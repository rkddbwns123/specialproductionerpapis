package shop.usehwa.gen.api.work.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.work.entity.Contract;
import shop.usehwa.gen.api.work.entity.WorkStatus;
import shop.usehwa.gen.common.function.ConvertFormat;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CustomerViewResponse {
    private String customerName;

    private String contractNumber;

    private String contractDate;

    private String packageName;

    private String currentProgress;

    private List<CustomerViewItem> processStatus;

    private CustomerViewResponse(CustomerViewResponseBuilder builder) {
        this.customerName = builder.customerName;
        this.contractNumber = builder.contractNumber;
        this.contractDate = builder.contractDate;
        this.packageName = builder.packageName;
        this.currentProgress = builder.currentProgress;
        this.processStatus = builder.processStatus;
    }

    public static class CustomerViewResponseBuilder implements CommonModelBuilder<CustomerViewResponse> {

        private final String customerName;
        private final String contractNumber;
        private final String contractDate;
        private final String packageName;
        private final String currentProgress;
        private final List<CustomerViewItem> processStatus;

        public CustomerViewResponseBuilder(Contract contract, String currentProgress, List<CustomerViewItem> processStatus) {
            this.customerName = contract.getCustomerName();
            this.contractNumber = contract.getContractNumber();
            this.contractDate = ConvertFormat.convertLocalDateToString(contract.getContractDate());
            this.packageName = contract.getPackageInfo().getPackageName();
            this.currentProgress = currentProgress;
            this.processStatus = processStatus;
        }

        @Override
        public CustomerViewResponse build() {
            return new CustomerViewResponse(this);
        }
    }
}
