package shop.usehwa.gen.api.work.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.work.entity.PackageInfo;

public interface PackageInfoRepository extends JpaRepository<PackageInfo, Long> {
}
