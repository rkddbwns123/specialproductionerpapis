package shop.usehwa.gen.api.work.model;

import lombok.Getter;
import lombok.Setter;
import shop.usehwa.gen.api.work.entity.WorkRecord;

@Getter
@Setter
public class WorkFinishRequest {
    private String approver;
}
