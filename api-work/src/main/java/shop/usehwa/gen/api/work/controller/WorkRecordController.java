package shop.usehwa.gen.api.work.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import shop.usehwa.gen.api.work.model.WorkRecordCreateRequest;
import shop.usehwa.gen.api.work.model.WorkRecordItem;
import shop.usehwa.gen.api.work.service.WorkRecordService;
import shop.usehwa.gen.common.response.model.CommonResult;
import shop.usehwa.gen.common.response.model.ListResult;
import shop.usehwa.gen.common.response.service.ResponseService;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "업무 기록 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/work/record")
public class WorkRecordController {
    private final WorkRecordService workRecordService;

    @ApiOperation(value = "업무기록 등록")
    @PostMapping("/new/{teamId}/{contractId}")
    public CommonResult setWorkRecord(
            @PathVariable long teamId,
            @PathVariable long contractId,
            @RequestBody @Valid WorkRecordCreateRequest createRequest) {
        workRecordService.setWorkRecord(createRequest, teamId, contractId);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "전체 업무기록 불러오기")
    @GetMapping("/all")
    public ListResult<WorkRecordItem> getWorkRecordList() {
        return ResponseService.getListResult(workRecordService.getWorkRecordList(), true);
    }

    @ApiOperation(value = "팀별 업무기록 가져오기")
    @GetMapping("/team")
    public ListResult<WorkRecordItem> getListByTeam(@RequestParam long teamId) {
        return ResponseService.getListResult(workRecordService.getListByTeam(teamId), true);
    }

    @ApiOperation(value = "계약 목록별 업무기록 불러오기")
    @GetMapping("/contract/{contractId}")
    public ListResult<WorkRecordItem> getListByContractId(@PathVariable long contractId) {
        return ResponseService.getListResult(workRecordService.getListByContractId(contractId), true);
    }

    @ApiOperation(value = "날짜 범위 내의 업무기록 불러오기")
    @GetMapping("/date")
    public ListResult<WorkRecordItem> getListByDateBetween(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate) {
        return ResponseService.getListResult(workRecordService.getListByDateBetween(startDate, endDate), true);
    }
}
