package shop.usehwa.gen.api.work.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.work.entity.WorkStatus;
import shop.usehwa.gen.common.function.ConvertFormat;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WorkStatusItem {
    private String processName;

    private String teamName;

    private String completeState;

    private String contractNumber;

    private String packageName;

    private WorkStatusItem(WorkStatusItemBuilder builder) {
        this.processName = builder.processName;
        this.teamName = builder.teamName;
        this.completeState = builder.completeState;
        this.contractNumber = builder.contractNumber;
        this.packageName = builder.packageName;
    }

    public static class WorkStatusItemBuilder implements CommonModelBuilder<WorkStatusItem> {

        private final String processName;
        private final String teamName;
        private final String completeState;
        private final String contractNumber;
        private final String packageName;

        public WorkStatusItemBuilder(WorkStatus workStatus) {
            this.processName = workStatus.getProcess().getProcessName();
            this.teamName = workStatus.getTeam().getTeamName();
            this.completeState = workStatus.getIsComplete() ? "완료" : "진행 중";
            this.contractNumber = workStatus.getContract().getContractNumber();
            this.packageName = workStatus.getContract().getPackageInfo().getPackageName();
        }

        @Override
        public WorkStatusItem build() {
            return new WorkStatusItem(this);
        }
    }
}
