package shop.usehwa.gen.api.work.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.work.entity.Contract;

import java.time.LocalDate;
import java.util.List;

public interface ContractRepository extends JpaRepository<Contract, Long> {
    List<Contract> findByEndWorkDateIsNull();
    List<Contract> findByEndWorkDateIsNotNull();
    Contract findByContractNumber(String contractNumber);
    List<Contract> findByContractDateBetween(LocalDate startDate, LocalDate endDate);

    Contract findByContractNumberAndCustomerPhone(String contractNumber, String customerPhone);
}
