package shop.usehwa.gen.api.work.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.work.entity.WorkRecord;
import shop.usehwa.gen.common.function.ConvertFormat;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WorkRecordItem {
    private Long workRecordId;
    private String contractNum;
    private String contractDate;
    private String packageName;
    private String processName;
    private String teamName;
    private String imageAddress;
    private String memo;
    private String createDate;
    private String workerName;

    private WorkRecordItem(WorkRecordItemBuilder builder) {
        this.workRecordId = builder.workRecordId;
        this.contractNum = builder.contractNum;
        this.contractDate = builder.contractDate;
        this.packageName = builder.packageName;
        this.processName = builder.processName;
        this.teamName = builder.teamName;
        this.imageAddress = builder.imageAddress;
        this.memo = builder.memo;
        this.createDate = builder.createDate;
        this.workerName = builder.workerName;
    }

    public static class WorkRecordItemBuilder implements CommonModelBuilder<WorkRecordItem> {
        private final Long workRecordId;
        private final String contractNum;
        private final String contractDate;
        private final String packageName;
        private final String processName;
        private final String teamName;
        private final String imageAddress;
        private final String memo;
        private final String createDate;
        private final String workerName;

        public WorkRecordItemBuilder(WorkRecord workRecord) {
            this.workRecordId = workRecord.getId();
            this.contractNum = workRecord.getContract().getContractNumber();
            this.contractDate = workRecord.getContract().getContractDate().toString();
            this.packageName = workRecord.getProcess().getPackageInfo().getPackageName();
            this.processName = workRecord.getProcess().getProcessName();
            this.teamName = workRecord.getTeam().getTeamName();
            this.imageAddress = workRecord.getImageAddress();
            this.memo = workRecord.getMemo();
            this.createDate = ConvertFormat.convertLocalDateTimeToStringMini(workRecord.getCreateDateTime());
            this.workerName = workRecord.getWorkerName();
        }

        @Override
        public WorkRecordItem build() {
            return new WorkRecordItem(this);
        }
    }
}
