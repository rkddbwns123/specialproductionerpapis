package shop.usehwa.gen.api.work.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class ContractCreateRequest {
    @ApiModelProperty(notes = "계약 번호 / 5 ~ 20자")
    @NotNull
    @Length(min = 5, max = 20)
    private String contractNumber;

    @ApiModelProperty(notes = "고객명 / 2 ~ 20자")
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @ApiModelProperty(notes = "고객 전화번호 / 13자")
    @NotNull
    @Length(min = 13, max = 13)
    private String customerPhone;

    @ApiModelProperty(notes = "계약일자")
    @NotNull
    private LocalDate contractDate;
}
