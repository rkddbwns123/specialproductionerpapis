package shop.usehwa.gen.api.work.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.work.entity.Contract;
import shop.usehwa.gen.common.function.ConvertFormat;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ContractItem {

    private String packageName;

    private String presentProcessName;

    private Integer presentProcessOrder;

    private String contractNumber;

    private String customerName;

    private String contractDate;

    private ContractItem(ContractItemBuilder builder) {
        this.packageName = builder.packageName;
        this.presentProcessName = builder.presentProcessName;
        this.presentProcessOrder = builder.presentProcessOrder;
        this.contractNumber = builder.contractNumber;
        this.customerName = builder.customerName;
        this.contractDate = builder.contractDate;
    }

    public static class ContractItemBuilder implements CommonModelBuilder<ContractItem> {

        private final String packageName;
        private final String presentProcessName;
        private final Integer presentProcessOrder;
        private final String contractNumber;
        private final String customerName;
        private final String contractDate;

        public ContractItemBuilder(Contract contract) {
            this.packageName = contract.getPackageInfo().getPackageName();
            this.presentProcessName = contract.getPresentProcess().getProcessName();
            this.presentProcessOrder = contract.getPresentProcess().getProcessOrder();
            this.contractNumber = contract.getContractNumber();
            this.customerName = contract.getCustomerName();
            this.contractDate = ConvertFormat.convertLocalDateToString(contract.getContractDate());
        }
        @Override
        public ContractItem build() {
            return new ContractItem(this);
        }
    }
}
