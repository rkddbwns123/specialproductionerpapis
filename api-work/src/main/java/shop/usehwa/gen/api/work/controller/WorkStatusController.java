package shop.usehwa.gen.api.work.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import shop.usehwa.gen.api.work.model.WorkStatusItem;
import shop.usehwa.gen.api.work.model.WorkStatusMiniResponse;
import shop.usehwa.gen.api.work.model.WorkStatusResponse;
import shop.usehwa.gen.api.work.service.WorkStatusService;
import shop.usehwa.gen.common.response.model.ListResult;
import shop.usehwa.gen.common.response.model.SingleResult;
import shop.usehwa.gen.common.response.service.ResponseService;

import java.time.LocalDate;

@Api(tags = "업무 현황 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/work/status")
public class WorkStatusController {
    private final WorkStatusService workStatusService;

    @ApiOperation(value = "전체 업무 가져오기")
    @GetMapping("/all")
    public ListResult<WorkStatusItem> getList() {
        return ResponseService.getListResult(workStatusService.getList(),true);
    }

    @ApiOperation(value = "팀 id로 업무현황 가져오기")
    @GetMapping("/team")
    public ListResult<WorkStatusItem> getListByTeam(@RequestParam long teamId) {
        return ResponseService.getListResult(workStatusService.getListByTeam(teamId), true);
    }

    @ApiOperation(value = "승인자 별로 업무현황 가져오기")
    @GetMapping("/approver")
    public ListResult<WorkStatusItem> getListByApprover(@RequestParam String approver) {
        return ResponseService.getListResult(workStatusService.getListByApprover(approver), true);
    }

    @ApiOperation(value = "날짜 범위 내의 업무현황 가져오기")
    @GetMapping("/date")
    public ListResult<WorkStatusItem> getListByDateBetween(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate) {
        return ResponseService.getListResult(workStatusService.getListByDateBetween(startDate, endDate), true);
    }

    @ApiOperation(value = "업무 현황 상세보기 (완료 후)")
    @GetMapping("/detail/work-status-id/{workStatusId}")
    public SingleResult<WorkStatusResponse> getWorkStatus(@PathVariable long workStatusId) {
        return ResponseService.getSingleResult(workStatusService.getWorkStatus(workStatusId));
    }

    @ApiOperation(value = "업무 현황 간략하게 보기 (완료 전)")
    @GetMapping("/mini/work-status-id/{workStatusId}")
    public SingleResult<WorkStatusMiniResponse> getWorkStatusMini(@PathVariable long workStatusId) {
        return ResponseService.getSingleResult(workStatusService.getWorkStatusMini(workStatusId));
    }
}
