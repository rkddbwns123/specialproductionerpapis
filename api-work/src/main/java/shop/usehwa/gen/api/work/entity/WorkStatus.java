package shop.usehwa.gen.api.work.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.work.model.ContractCreateRequest;
import shop.usehwa.gen.api.work.model.WorkFinishRequest;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WorkStatus {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "공정 관리 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "processId")
    private Process process;

    @ApiModelProperty(notes = "팀 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "teamId")
    private Team team;

    @ApiModelProperty(notes = "계약서 관리 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "contractId")
    private Contract contract;

    @ApiModelProperty(notes = "완료 여부")
    @Column(nullable = false)
    private Boolean isComplete;

    @ApiModelProperty(notes = "업무 기록 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mainWorkRecordId")
    private WorkRecord workRecord;

    @ApiModelProperty(notes = "승인자")
    @Column(length = 20)
    private String approver;

    @ApiModelProperty(notes = "등록일자")
    @Column(nullable = false)
    private LocalDateTime createDate;

    @ApiModelProperty(notes = "종료일자")
    private LocalDateTime finishDate;

    public void finishWork(WorkFinishRequest request, WorkRecord workRecord) {
        this.isComplete = true;
        this.workRecord = workRecord;
        this.approver = request.getApprover();
        this.finishDate = LocalDateTime.now();
    }

    private WorkStatus(WorkStatusBuilder builder) {
        this.process = builder.process;
        this.team = builder.team;
        this.contract = builder.contract;
        this.isComplete = builder.isComplete;
        this.workRecord = builder.workRecord;
        this.approver = builder.approver;
        this.createDate = builder.createDate;
        this.finishDate = builder.finishDate;
    }

    public static class WorkStatusBuilder implements CommonModelBuilder<WorkStatus> {

        private final Process process;
        private final Team team;
        private final Contract contract;
        private final Boolean isComplete;
        private final WorkRecord workRecord;
        private final String approver;
        private final LocalDateTime createDate;
        private final LocalDateTime finishDate;

        public WorkStatusBuilder(Contract contract) {
            this.process = contract.getPresentProcess();
            this.team = contract.getPresentProcess().getTeam();
            this.contract = contract;
            this.isComplete = false;
            this.workRecord = null;
            this.approver = null;
            this.createDate = LocalDateTime.now();
            this.finishDate = null;
        }

        @Override
        public WorkStatus build() {
            return new WorkStatus(this);
        }
    }
}
