package shop.usehwa.gen.api.work.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import shop.usehwa.gen.api.work.model.CustomerViewItem;
import shop.usehwa.gen.api.work.model.CustomerViewResponse;
import shop.usehwa.gen.api.work.service.CustomerViewService;
import shop.usehwa.gen.common.response.model.SingleResult;
import shop.usehwa.gen.common.response.service.ResponseService;

@Api(tags = "고객용 진행도 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerViewController {
    private final CustomerViewService customerViewService;

    @ApiOperation(value = "고객용 진행도 확인")
    @GetMapping("/view")
    public SingleResult<CustomerViewResponse> getCustomerView(@RequestParam String contractNumber, @RequestParam String customerPhone) {
        return ResponseService.getSingleResult(customerViewService.getCustomerView(contractNumber, customerPhone));
    }
}
