package shop.usehwa.gen.api.work.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.work.entity.Contract;
import shop.usehwa.gen.api.work.entity.PackageInfo;
import shop.usehwa.gen.api.work.entity.Process;
import shop.usehwa.gen.api.work.entity.WorkStatus;
import shop.usehwa.gen.api.work.model.ContractCreateRequest;
import shop.usehwa.gen.api.work.model.ContractItem;
import shop.usehwa.gen.api.work.model.ContractPhoneUpdateRequest;
import shop.usehwa.gen.api.work.model.ContractResponse;
import shop.usehwa.gen.api.work.repository.ContractRepository;
import shop.usehwa.gen.api.work.repository.PackageInfoRepository;
import shop.usehwa.gen.api.work.repository.ProcessRepository;
import shop.usehwa.gen.api.work.repository.WorkStatusRepository;
import shop.usehwa.gen.common.exception.*;
import shop.usehwa.gen.common.response.model.ListResult;
import shop.usehwa.gen.common.response.service.ListConvertService;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ContractService {
    private final ContractRepository contractRepository;
    private final WorkStatusRepository workStatusRepository;
    private final PackageInfoRepository packageInfoRepository;
    private final ProcessRepository processRepository;

    public void setContract(ContractCreateRequest request, long packageInfoId) {
        PackageInfo packageInfo = packageInfoRepository.findById(packageInfoId).orElseThrow(CMissingDataException::new);
        List<Process> processList = processRepository.findByPackageInfoOrderByProcessOrderAsc(packageInfo);

        Process process = processList.get(0);

        Contract addData1 = new Contract.ContractBuilder(request, packageInfo, process).build();

        contractRepository.save(addData1);

        WorkStatus addData2 = new WorkStatus.WorkStatusBuilder(addData1).build();

        workStatusRepository.save(addData2);

    }

    public void putContractPhoneNumber(long id, ContractPhoneUpdateRequest request) {
        Contract originData = contractRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putCustomerPhone(request);

        contractRepository.save(originData);
    }

    public ListResult<ContractItem> getContractList() {
        List<Contract> originList = contractRepository.findAll();

        List<ContractItem> result = new LinkedList<>();

        if (originList.isEmpty()) throw new CNotRegistrationContractException();
        originList.forEach(item -> result.add(new ContractItem.ContractItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<ContractItem> getProgressContractList() {
        List<Contract> originList = contractRepository.findByEndWorkDateIsNull();

        List<ContractItem> result = new LinkedList<>();

        if (originList.isEmpty()) throw new CDontFindProgressContractException();
        originList.forEach(item -> result.add(new ContractItem.ContractItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ContractResponse getContract(String contractNumber) {
        Contract originData = contractRepository.findByContractNumber(contractNumber);

        if (originData == null) throw new CDontFindContractException();
        return new ContractResponse.ContractResponseBuilder(originData).build();
    }

    public ListResult<ContractItem> getFinishContractList() {
        List<Contract> originList = contractRepository.findByEndWorkDateIsNotNull();

        List<ContractItem> result = new LinkedList<>();

        if (originList.isEmpty()) throw new CDontFindFinishContractException();
        originList.forEach(item -> result.add(new ContractItem.ContractItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<ContractItem> getBetweenDateList(LocalDate startDate, LocalDate endDate) {
        List<Contract> originList = contractRepository.findByContractDateBetween(startDate, endDate);

        List<ContractItem> result = new LinkedList<>();

        if (originList.isEmpty()) throw new CDontFindContractException();
        originList.forEach(item -> result.add(new ContractItem.ContractItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
}
