package shop.usehwa.gen.api.work.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.work.entity.*;
import shop.usehwa.gen.api.work.model.WorkRecordCreateRequest;
import shop.usehwa.gen.api.work.model.WorkRecordItem;
import shop.usehwa.gen.api.work.repository.*;
import shop.usehwa.gen.common.exception.CDontFindTeamInfoException;
import shop.usehwa.gen.common.exception.CNotRegistrationContractException;
import shop.usehwa.gen.common.exception.CNotRegistrationWorkRecordException;
import shop.usehwa.gen.common.response.model.ListResult;
import shop.usehwa.gen.common.response.service.ListConvertService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WorkRecordService {
    private final WorkRecordRepository workRecordRepository;
    private final TeamRepository teamRepository;
    private final ContractRepository contractRepository;

    public void setWorkRecord(WorkRecordCreateRequest createRequest, long teamId, long contractId) {
        Team team = teamRepository.findById(teamId).orElseThrow(CDontFindTeamInfoException::new);
        Contract contract = contractRepository.findById(contractId).orElseThrow(CNotRegistrationContractException::new);
        WorkRecord addData = new WorkRecord.WorkRecordBuilder(createRequest, team, contract).build();

        workRecordRepository.save(addData);
    }

    public ListResult<WorkRecordItem> getWorkRecordList() {
        List<WorkRecord> originList = workRecordRepository.findAll();

        if (originList.isEmpty()) throw new CNotRegistrationWorkRecordException();

        List<WorkRecordItem> result = new LinkedList<>();
        originList.forEach(item -> result.add(new WorkRecordItem.WorkRecordItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<WorkRecordItem> getListByTeam(long teamId) {
        List<WorkRecord> originList = workRecordRepository.findByTeam_Id(teamId);

        if (originList.isEmpty()) throw new CNotRegistrationWorkRecordException();
        List<WorkRecordItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new WorkRecordItem.WorkRecordItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<WorkRecordItem> getListByContractId(long contractId) {
        List<WorkRecord> originList = workRecordRepository.findByContract_Id(contractId);

        if (originList.isEmpty()) throw new CNotRegistrationWorkRecordException();
        List<WorkRecordItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new WorkRecordItem.WorkRecordItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<WorkRecordItem> getListByDateBetween(LocalDate startDate, LocalDate endDate) {
        LocalDateTime startDateTime = startDate.atTime(0,0,0);
        LocalDateTime endDateTime = endDate.atTime(23,59,59);

        List<WorkRecord> originList = workRecordRepository.findByCreateDateTimeBetween(startDateTime, endDateTime);

        if (originList.isEmpty()) throw new CNotRegistrationWorkRecordException();

        List<WorkRecordItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new WorkRecordItem.WorkRecordItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
}
