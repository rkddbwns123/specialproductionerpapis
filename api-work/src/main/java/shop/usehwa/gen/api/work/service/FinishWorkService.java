package shop.usehwa.gen.api.work.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.work.entity.Contract;
import shop.usehwa.gen.api.work.entity.PackageInfo;
import shop.usehwa.gen.api.work.entity.Process;
import shop.usehwa.gen.api.work.entity.WorkRecord;
import shop.usehwa.gen.api.work.entity.WorkStatus;
import shop.usehwa.gen.api.work.model.WorkFinishRequest;
import shop.usehwa.gen.api.work.repository.ContractRepository;
import shop.usehwa.gen.api.work.repository.ProcessRepository;
import shop.usehwa.gen.api.work.repository.WorkRecordRepository;
import shop.usehwa.gen.api.work.repository.WorkStatusRepository;
import shop.usehwa.gen.common.exception.CNotRegistrationWorkRecordException;
import shop.usehwa.gen.common.exception.CNotRegistrationWorkStatusException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FinishWorkService {
    private final ContractRepository contractRepository;
    private final ProcessRepository processRepository;
    private final WorkStatusRepository workStatusRepository;
    private final WorkRecordRepository workRecordRepository;

    public void putFinishWork(WorkFinishRequest request, long workStatusId, long workRecordId) {
        WorkStatus workStatus = workStatusRepository.findById(workStatusId).orElseThrow(CNotRegistrationWorkStatusException::new);
        WorkRecord workRecord = workRecordRepository.findById(workRecordId).orElseThrow(CNotRegistrationWorkRecordException::new);
        workStatus.finishWork(request, workRecord);

        workStatusRepository.save(workStatus);

        Contract contract = workStatus.getContract();
        PackageInfo packageInfo = contract.getPackageInfo();

        List<Process> processList = processRepository.findByPackageInfoOrderByProcessOrderAsc(packageInfo);

        List<WorkStatus> workStatusList = workStatusRepository.findAllByContract(contract);
        if (processList.size() != workStatusList.size()) {
            Process process = processList.get(contract.getPresentProcess().getProcessOrder());

            contract.finishWork(process);

            contractRepository.save(contract);

            WorkStatus addData = new WorkStatus.WorkStatusBuilder(contract).build();
            workStatusRepository.save(addData);
        } else {
            contract.putEndWorkDate();
            contractRepository.save(contract);
        }
    }
}
