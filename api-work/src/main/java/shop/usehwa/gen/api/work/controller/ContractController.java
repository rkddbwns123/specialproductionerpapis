package shop.usehwa.gen.api.work.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import shop.usehwa.gen.api.work.model.ContractCreateRequest;
import shop.usehwa.gen.api.work.model.ContractItem;
import shop.usehwa.gen.api.work.model.ContractPhoneUpdateRequest;
import shop.usehwa.gen.api.work.model.ContractResponse;
import shop.usehwa.gen.api.work.service.ContractService;
import shop.usehwa.gen.common.response.model.CommonResult;
import shop.usehwa.gen.common.response.model.ListResult;
import shop.usehwa.gen.common.response.model.SingleResult;
import shop.usehwa.gen.common.response.service.ResponseService;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "계약 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/contract")
public class ContractController {
    private final ContractService contractService;

    @ApiOperation(value = "계약서 등록 / 업무 등록")
    @PostMapping("/new/{packageInfoId}")
    public CommonResult setContract(@PathVariable long packageInfoId,
                                    @RequestBody @Valid ContractCreateRequest request) {
        contractService.setContract(request, packageInfoId);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "계약서 고객 전화번호 수정")
    @PutMapping("/update/{id}")
    public CommonResult putCustomerPhone(@PathVariable long id, @RequestBody ContractPhoneUpdateRequest request) {
        contractService.putContractPhoneNumber(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "계약서 전체 목록")
    @GetMapping("/all")
    public ListResult<ContractItem> getList() {
        return ResponseService.getListResult(contractService.getContractList(), true);
    }

    @ApiOperation(value = "진행중인 계약서 목록")
    @GetMapping("/progress")
    public ListResult<ContractItem> getProgressContractList() {
        return ResponseService.getListResult(contractService.getProgressContractList(), true);
    }

    @ApiOperation(value = "계약번호로 계약서 찾기")
    @GetMapping("/number")
    public SingleResult<ContractResponse> getContract(@RequestParam String contractNumber) {
        return ResponseService.getSingleResult(contractService.getContract(contractNumber));
    }

    @ApiOperation(value = "완료된 계약서 목록")
    @GetMapping("/finish")
    public ListResult<ContractItem> getFinishContractList() {
        return ResponseService.getListResult(contractService.getFinishContractList(), true);
    }

    @ApiOperation(value = "지정 날짜 범위 내의 계약서 목록")
    @GetMapping("/date")
    public ListResult<ContractItem> getBetweenDateList(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate) {
        return ResponseService.getListResult(contractService.getBetweenDateList(startDate, endDate), true);
    }
}
