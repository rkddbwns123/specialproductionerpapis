package shop.usehwa.gen.api.work.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Team {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "팀명")
    @Column(nullable = false, length = 20)
    private String teamName;

    @ApiModelProperty(notes = "주 업무")
    @Column(nullable = false, length = 50)
    private String mainWork;

    @ApiModelProperty(notes = "팀장")
    @Column(nullable = false, length = 20)
    private String teamLeader;

    @ApiModelProperty(notes = "사용 중 여부")
    @Column(nullable = false)
    private Boolean isUse;

    @ApiModelProperty(notes = "등록일자")
    @Column(nullable = false)
    private LocalDateTime createDate;

}
