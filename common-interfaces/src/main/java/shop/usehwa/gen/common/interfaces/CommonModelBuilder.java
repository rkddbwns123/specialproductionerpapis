package shop.usehwa.gen.common.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
