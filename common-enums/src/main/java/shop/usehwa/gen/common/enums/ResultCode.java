package shop.usehwa.gen.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0, "성공하였습니다.")
    , FAILED(-1, "실패하였습니다.")

    , ACCESS_DENIED(-1000, "권한이 없습니다.")
    , USERNAME_SIGN_IN_FAILED(-1001, "가입된 사용자가 아닙니다.")
    , AUTHENTICATION_ENTRY_POINT(-1002, "접근 권한이 없습니다.")

    , DUPLICATION_USERNAME(-400, "중복된 아이디입니다.")
    , WRONG_PASSWORD(-401,"비밀번호가 일치하지 않습니다.")
    , WRONG_USERNAME_TYPE(-402,"유효한 아이디 형식이 아닙니다.")

    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.")
    , WRONG_PHONE_NUMBER(-10001, "잘못된 핸드폰 번호입니다.")

    , DONT_FIND_CONTRACT(-11000, "계약서 정보를 찾을 수 없습니다.")
    , DONT_FIND_FINISH_CONTRACT(-11001, "완료된 계약이 없습니다.")
    , NOT_REGISTRATION_CONTRACT(-11002,"등록된 계약서 정보가 없습니다.")
    , DONT_FIND_PROGRESS_CONTRACT(-11003,"진행중인 계약이 없습니다.")

    , DONT_FIND_NOT_USING_PACKAGE_INFO(-12000, "활성화된 패키지가 없습니다.")
    , NOT_REGISTRATION_PACKAGE_INFO(-12001, "등록된 패키지 정보가 없습니다.")

    , DONT_FIND_NOT_USING_PROCESS_INFO(-13000, "활성화된 공정이 없습니다.")
    , NOT_REGISTRATION_PROCESS_INFO(-13001, "등록된 공정 정보가 없습니다.")
    , DONT_FIND_PROCESS_INFO(-13002,"조건에 맞는 공정이 없습니다.")

    , DONT_FIND_WORK_STATUS(-14000, "조건에 맞는 업무가 없습니다.")
    , NOT_REGISTRATION_WORK_STATUS(-14001, "등록된 업무가 없습니다.")

    , NOT_REGISTRATION_WORK_RECORD(-15000, "등록된 업무 기록이 없습니다.")

    , DONT_FIND_TEAM_INFO(-16000, "등록된 팀 정보가 없습니다.")
    , DONT_FIND_MEMBER_INFO(-17000, "등록된 회원 정보가 없습니다.")

    ;

    private final Integer code;
    private final String msg;
}

