package shop.usehwa.gen.common.exception;

public class CDontFindMemberInfoException extends RuntimeException {
    public CDontFindMemberInfoException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDontFindMemberInfoException(String msg) {
        super(msg);
    }

    public CDontFindMemberInfoException() {
        super();
    }
}
