package shop.usehwa.gen.common.exception;

public class CNotRegistrationContractException extends RuntimeException {
    public CNotRegistrationContractException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotRegistrationContractException(String msg) {
        super(msg);
    }

    public CNotRegistrationContractException() {
        super();
    }
}