package shop.usehwa.gen.common.exception;

public class CNotRegistrationProcessInfoException extends RuntimeException {
    public CNotRegistrationProcessInfoException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotRegistrationProcessInfoException(String msg) {
        super(msg);
    }

    public CNotRegistrationProcessInfoException() {
        super();
    }
}