package shop.usehwa.gen.common.exception;

public class CNotRegistrationWorkRecordException extends RuntimeException {
    public CNotRegistrationWorkRecordException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotRegistrationWorkRecordException(String msg) {
        super(msg);
    }

    public CNotRegistrationWorkRecordException() {
        super();
    }
}