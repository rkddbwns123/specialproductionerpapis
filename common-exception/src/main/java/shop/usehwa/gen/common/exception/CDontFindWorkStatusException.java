package shop.usehwa.gen.common.exception;

public class CDontFindWorkStatusException extends RuntimeException {
    public CDontFindWorkStatusException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDontFindWorkStatusException(String msg) {
        super(msg);
    }

    public CDontFindWorkStatusException() {
        super();
    }
}