package shop.usehwa.gen.common.exception;

public class CDontFindProgressContractException extends RuntimeException {
    public CDontFindProgressContractException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDontFindProgressContractException(String msg) {
        super(msg);
    }

    public CDontFindProgressContractException() {
        super();
    }
}