package shop.usehwa.gen.common.exception;

public class CNotRegistrationWorkStatusException extends RuntimeException {
    public CNotRegistrationWorkStatusException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotRegistrationWorkStatusException(String msg) {
        super(msg);
    }

    public CNotRegistrationWorkStatusException() {
        super();
    }
}