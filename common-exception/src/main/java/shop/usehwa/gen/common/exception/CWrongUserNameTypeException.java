package shop.usehwa.gen.common.exception;

public class CWrongUserNameTypeException extends RuntimeException {
    public CWrongUserNameTypeException(String msg, Throwable t) {
        super(msg, t);
    }

    public CWrongUserNameTypeException(String msg) {
        super(msg);
    }

    public CWrongUserNameTypeException() {
        super();
    }
}