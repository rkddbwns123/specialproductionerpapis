package shop.usehwa.gen.common.exception;

public class CDontFindProcessInfoException extends RuntimeException {
    public CDontFindProcessInfoException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDontFindProcessInfoException(String msg) {
        super(msg);
    }

    public CDontFindProcessInfoException() {
        super();
    }
}