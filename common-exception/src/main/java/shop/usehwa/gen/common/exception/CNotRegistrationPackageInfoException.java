package shop.usehwa.gen.common.exception;

public class CNotRegistrationPackageInfoException extends RuntimeException {
    public CNotRegistrationPackageInfoException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotRegistrationPackageInfoException(String msg) {
        super(msg);
    }

    public CNotRegistrationPackageInfoException() {
        super();
    }
}