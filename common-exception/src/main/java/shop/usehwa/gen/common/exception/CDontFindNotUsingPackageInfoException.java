package shop.usehwa.gen.common.exception;

public class CDontFindNotUsingPackageInfoException extends RuntimeException {
    public CDontFindNotUsingPackageInfoException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDontFindNotUsingPackageInfoException(String msg) {
        super(msg);
    }

    public CDontFindNotUsingPackageInfoException() {
        super();
    }
}