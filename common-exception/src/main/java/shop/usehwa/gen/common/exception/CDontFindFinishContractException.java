package shop.usehwa.gen.common.exception;

public class CDontFindFinishContractException extends RuntimeException {
    public CDontFindFinishContractException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDontFindFinishContractException(String msg) {
        super(msg);
    }

    public CDontFindFinishContractException() {
        super();
    }
}