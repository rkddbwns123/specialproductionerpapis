package shop.usehwa.gen.common.exception;

public class CDontFindTeamInfoException extends RuntimeException {
    public CDontFindTeamInfoException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDontFindTeamInfoException(String msg) {
        super(msg);
    }

    public CDontFindTeamInfoException() {
        super();
    }
}