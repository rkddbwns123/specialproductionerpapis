package shop.usehwa.gen.api.process.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProcessInfoUpdateRequest {
    @ApiModelProperty(notes = "팀 ID", required = true)
    @NotNull
    private Long teamId;

    @ApiModelProperty(notes = "패키지 ID", required = true)
    @NotNull
    private Long packageInfoId;
}
