package shop.usehwa.gen.api.process.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.process.model.PackageInfoCreateRequest;
import shop.usehwa.gen.api.process.model.PackageInfoUseRequest;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PackageInfo {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "패키지명")
    @Column(nullable = false, length = 20)
    private String packageName;

    @ApiModelProperty(notes = "사용 중 여부")
    @Column(nullable = false)
    private Boolean isUse;

    @ApiModelProperty(notes = "등록일자")
    @Column(nullable = false)
    private LocalDateTime createDate;

    @ApiModelProperty(notes = "메모")
    @Column(columnDefinition = "TEXT")
    private String memo;

    public void putPackageInfo(PackageInfoCreateRequest request) {
        this.packageName = request.getPackageName();
        this.memo = request.getMemo();
    }

    public void putPackageInfoUse(PackageInfoUseRequest request) {
        this.isUse = request.getIsUse();
    }

    private PackageInfo(PackageInfoBuilder builder) {
        this.packageName = builder.packageName;
        this.isUse = builder.isUse;
        this.createDate = builder.createDate;
        this.memo = builder.memo;
    }

    public static class PackageInfoBuilder implements CommonModelBuilder<PackageInfo>{
        private final String packageName;
        private final Boolean isUse;
        private final LocalDateTime createDate;
        private String memo;

        public PackageInfoBuilder(PackageInfoCreateRequest request) {
            this.packageName = request.getPackageName();
            this.isUse = true;
            this.createDate = LocalDateTime.now();
        }

        public PackageInfoBuilder setMemo(String memo) {
            this.memo = memo;

            return this;
        }

        @Override
        public PackageInfo build() {
            return new PackageInfo(this);
        }
    }
}
