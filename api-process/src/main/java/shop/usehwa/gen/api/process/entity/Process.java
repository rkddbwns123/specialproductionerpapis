package shop.usehwa.gen.api.process.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.process.model.ProcessCreateRequest;
import shop.usehwa.gen.api.process.model.ProcessUseUpdaterequest;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Process {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "팀 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "teamId")
    private Team team;

    @ApiModelProperty(notes = "공정 이름")
    @Column(nullable = false, length = 50)
    private String processName;

    @ApiModelProperty(notes = "공정 순서")
    @Column(nullable = false)
    private Integer processOrder;

    @ApiModelProperty(notes = "패키지 관리 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "packageInfoId")
    private PackageInfo packageInfo;

    @ApiModelProperty(notes = "사용 중 여부")
    @Column(nullable = false)
    private Boolean isUse;

    @ApiModelProperty(notes = "등록일자")
    @Column(nullable = false)
    private LocalDateTime createDate;

    @ApiModelProperty(notes = "메모")
    @Column(columnDefinition = "TEXT")
    private String memo;

    public void putProcessInfo(Team team, PackageInfo packageInfo) {
        this.team = team;
        this.packageInfo = packageInfo;
    }

    public void putProcessUse(ProcessUseUpdaterequest request) {
        this.isUse = request.getIsUse();
    }

    private Process(Builder builder) {
        this.team = builder.team;
        this.processName = builder.processName;
        this.processOrder = builder.processOrder;
        this.packageInfo = builder.packageInfo;
        this.isUse = builder.isUse;
        this.createDate = builder.createDate;
        this.memo = builder.memo;
    }

    public static class Builder implements CommonModelBuilder<Process> {
        private final Team team;
        private final String processName;
        private final Integer processOrder;
        private final PackageInfo packageInfo;
        private final Boolean isUse;
        private final LocalDateTime createDate;
        private String memo;

        public Builder(Team team, PackageInfo packageInfo, ProcessCreateRequest request) {
            this.team = team;
            this.processName = request.getProcessName();
            this.processOrder = request.getProcessOrder();
            this.packageInfo = packageInfo;
            this.isUse = true;
            this.createDate = LocalDateTime.now();
        }

        public Builder setMemo(String memo) {
            this.memo = memo;

            return this;
        }

        @Override
        public Process build() {
            return new Process(this);
        }
    }
}
