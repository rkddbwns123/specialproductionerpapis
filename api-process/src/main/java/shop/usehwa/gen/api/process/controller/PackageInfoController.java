package shop.usehwa.gen.api.process.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.usehwa.gen.api.process.model.PackageInfoCreateRequest;
import shop.usehwa.gen.api.process.model.PackageInfoItem;
import shop.usehwa.gen.api.process.model.PackageInfoUseRequest;
import shop.usehwa.gen.api.process.service.PackageInfoService;
import shop.usehwa.gen.common.response.model.CommonResult;
import shop.usehwa.gen.common.response.model.ListResult;
import shop.usehwa.gen.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "패키지 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/packageInfo")
public class PackageInfoController {

    private final PackageInfoService packageInfoService;

    @ApiOperation(value = "패키지 등록")
    @PostMapping("/new")
    public CommonResult setPackageInfo(@RequestBody @Valid PackageInfoCreateRequest request) {
        packageInfoService.setPackageInfo(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "전체 패키지 목록")
    @GetMapping("/list/all")
    public ListResult<PackageInfoItem> getPackageInfos() {
        return ResponseService.getListResult( packageInfoService.getPackageInfos(),true);
    }

    @ApiOperation(value = "사용중인 패키지 목록")
    @GetMapping("/list/use")
    public ListResult<PackageInfoItem> getUsePackageInfos() {
        return ResponseService.getListResult( packageInfoService.getUsePackageInfos(),true);
    }

    @ApiOperation(value = "패키지 수정")
    @PutMapping("/update/info/{id}")
    public CommonResult putPackageInfo(@PathVariable long id, @RequestBody @Valid PackageInfoCreateRequest request) {
        packageInfoService.putPackageInfo(id, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "패키지 사용여부 수정")
    @PutMapping("/update/is-use")
    public CommonResult putPackageInfoUse(long id, @RequestBody @Valid PackageInfoUseRequest request) {
        packageInfoService.putPackageInfoUse(id, request);
        return ResponseService.getSuccessResult();
    }

}
