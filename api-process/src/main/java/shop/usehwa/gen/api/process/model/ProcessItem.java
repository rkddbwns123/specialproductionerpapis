package shop.usehwa.gen.api.process.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.process.entity.Process;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProcessItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long processId;

    @ApiModelProperty(notes = "팀명")
    private String teamName;

    @ApiModelProperty(notes = "패키지명")
    private String packageInfoName;

    @ApiModelProperty(notes = "공정 이름")
    private String processName;

    @ApiModelProperty(notes = "공정 순서")
    private Integer processOrder;

    @ApiModelProperty(notes = "사용 중 여부 한글 표시")
    private String isUseName;

    @ApiModelProperty(notes = "등록일자")
    private LocalDateTime createDate;

    @ApiModelProperty(notes = "메모")
    private String memo;

    private ProcessItem(Builder builder) {
        this.processId = builder.processId;
        this.teamName = builder.teamName;
        this.packageInfoName = builder.packageInfoName;
        this.processName = builder.processName;
        this.processOrder = builder.processOrder;
        this.isUseName = builder.isUseName;
        this.createDate = builder.createDate;
        this.memo = builder.memo;
    }

    public static class Builder implements CommonModelBuilder<ProcessItem> {
        private final Long processId;
        private final String teamName;
        private final String packageInfoName;
        private final String processName;
        private final Integer processOrder;
        private final String isUseName;
        private final LocalDateTime createDate;
        private String memo;

        public Builder(Process process) {
            this.processId = process.getId();
            this.teamName = process.getTeam().getTeamName();
            this.packageInfoName = process.getPackageInfo().getPackageName();
            this.processName = process.getProcessName();
            this.processOrder = process.getProcessOrder();
            this.isUseName = process.getIsUse()? "사용중" : "미사용";
            this.createDate = process.getCreateDate();
            this.memo = process.getMemo();
        }

        @Override
        public ProcessItem build() {
            return new ProcessItem(this);
        }
    }
}
