package shop.usehwa.gen.api.process.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.process.entity.PackageInfo;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
public class PackageInfoItem
{
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "패키지명")
    private String packageName;

    @ApiModelProperty(notes = "사용 중 여부")
    private Boolean isUse;

    @ApiModelProperty(notes = "등록일자")
    private LocalDateTime createDate;

    @ApiModelProperty(notes = "메모")
    private String memo;

    private PackageInfoItem(PackageInfoItemBuilder builder) {
        this.id = builder.id;
        this.packageName = builder.packageName;
        this.isUse = builder.isUse;
        this.createDate = builder.createDate;
        this.memo = builder.memo;
    }

    public static class PackageInfoItemBuilder implements CommonModelBuilder<PackageInfoItem> {
        private final Long id;
        private final String packageName;
        private final Boolean isUse;
        private final LocalDateTime createDate;
        private final String memo;

        public PackageInfoItemBuilder(PackageInfo packageInfo) {
            this.id = packageInfo.getId();
            this.packageName = packageInfo.getPackageName();
            this.isUse = packageInfo.getIsUse();
            this.createDate = packageInfo.getCreateDate();
            this.memo = packageInfo.getMemo();
        }

        @Override
        public PackageInfoItem build() {
            return new PackageInfoItem(this);
        }
    }

}
