package shop.usehwa.gen.api.process.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.process.entity.PackageInfo;
import shop.usehwa.gen.api.process.entity.Process;

import java.util.List;

public interface ProcessRepository extends JpaRepository<Process, Long> {
    List<Process> findAllByIsUse(Boolean isUse);
    List<Process> findAllByIsUseAndPackageInfo(Boolean isUse, PackageInfo packageInfo);
    List<Process> findByPackageInfo_PackageNameLike(String packageInfoName);
    List<Process> findByProcessNameLike(String processName);
}
