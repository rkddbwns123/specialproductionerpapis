package shop.usehwa.gen.api.process.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PackageInfoCreateRequest {

    @ApiModelProperty(value = "패키지명 (2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String packageName;

    @ApiModelProperty(value = "메모")
    private String memo;


}
