package shop.usehwa.gen.api.process.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.process.entity.PackageInfo;

import java.util.List;

public interface PackageInfoRepository extends JpaRepository<PackageInfo, Long> {

    List<PackageInfo> findByIsUse(Boolean isUse);
}
