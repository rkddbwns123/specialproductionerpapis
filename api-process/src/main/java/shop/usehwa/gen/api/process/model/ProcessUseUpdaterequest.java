package shop.usehwa.gen.api.process.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProcessUseUpdaterequest {
    @ApiModelProperty(notes = "사용 중 여부")
    @NotNull
    private Boolean isUse;
}
