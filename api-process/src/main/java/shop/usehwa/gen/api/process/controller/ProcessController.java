package shop.usehwa.gen.api.process.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.usehwa.gen.api.process.model.ProcessCreateRequest;
import shop.usehwa.gen.api.process.model.ProcessInfoUpdateRequest;
import shop.usehwa.gen.api.process.model.ProcessItem;
import shop.usehwa.gen.api.process.model.ProcessUseUpdaterequest;
import shop.usehwa.gen.api.process.service.ProcessService;
import shop.usehwa.gen.common.response.model.CommonResult;
import shop.usehwa.gen.common.response.model.ListResult;
import shop.usehwa.gen.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "공정 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/process")
public class ProcessController {
    private final ProcessService processService;

    @ApiOperation(value = "공정 등록")
    @PostMapping("/new")
    public CommonResult setProcess(@RequestBody @Valid ProcessCreateRequest request) {
        processService.setProcess(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "전체 공정 목록")
    @GetMapping("/all")
    public ListResult<ProcessItem> getAllProcess() {
        return ResponseService.getListResult(processService.getAllProcess(), true);
    }

    @ApiOperation(value = "사용중인 전체 공정 가져오기")
    @GetMapping("/all/use")
    public ListResult<ProcessItem> getUseProcess() {
        return ResponseService.getListResult(processService.getUseProcess(), true);
    }

    @ApiOperation(value = "패키지 별 사용중인 공정 가져오기")
    @GetMapping("/all/use/package-id/{id}")
    public ListResult<ProcessItem> getUsePackageProcess(@PathVariable long id) {
        return ResponseService.getListResult(processService.getUsePackageProcess(id), true);
    }

    @ApiOperation(value = "패키지명 검색하여 가져오기")
    @GetMapping("/search/package")
    public ListResult<ProcessItem> searchProcessByPackageInfo(@RequestParam("packageName") String packageName) {
        return ResponseService.getListResult(processService.searchProcessByPackageInfo(packageName), true);
    }

    @ApiOperation(value = "공정명 검색하여 가져오기")
    @GetMapping("/search/process")
    public ListResult<ProcessItem> searchProcessByProcessName(@RequestParam("processName") String processName) {
        return ResponseService.getListResult(processService.searchProcessByProcessName(processName), true);
    }

    @ApiOperation(value = "공정 정보 수정")
    @PutMapping("/info/process-id/{id}")
    public CommonResult putProcessInfo(@PathVariable long id, @RequestBody @Valid ProcessInfoUpdateRequest request) {
        processService.putProcessInfo(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "공정 활성화 여부 수정")
    @PutMapping("/use/process-id/{id}")
    public CommonResult putProcessUse(@PathVariable long id, @RequestBody @Valid ProcessUseUpdaterequest request) {
        processService.putProcessUse(id, request);

        return ResponseService.getSuccessResult();
    }
}
