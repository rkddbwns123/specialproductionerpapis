package shop.usehwa.gen.api.process.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shop.usehwa.gen.common.exception.CAccessDeniedException;
import shop.usehwa.gen.common.exception.CAuthenticationEntryPointException;
import shop.usehwa.gen.common.response.model.CommonResult;

@RestController
@RequestMapping("/exception")
public class ExceptionController {
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {
        throw new CAccessDeniedException();
    }

    @GetMapping("/entry-point")
    public CommonResult entryPointException() {
        throw new CAuthenticationEntryPointException();
    }

}
