package shop.usehwa.gen.api.process.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.process.entity.Team;

public interface TeamRepository extends JpaRepository<Team, Long> {
}
