package shop.usehwa.gen.api.process.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProcessCreateRequest {
    @ApiModelProperty(notes = "팀 ID", required = true)
    @NotNull
    private Long teamId;

    @ApiModelProperty(notes = "패키지 ID", required = true)
    @NotNull
    private Long packageInfoId;

    @ApiModelProperty(notes = "공정 이름 (~50)", required = true)
    @NotNull
    @Length(min = 1, max = 50)
    private String processName;

    @ApiModelProperty(notes = "공정 순서", required = true)
    @NotNull
    private Integer processOrder;

    @ApiModelProperty(notes = "메모")
    private String memo;
}
