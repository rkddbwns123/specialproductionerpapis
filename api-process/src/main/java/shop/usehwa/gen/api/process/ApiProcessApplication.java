package shop.usehwa.gen.api.process;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiProcessApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiProcessApplication.class, args);
    }
}
