package shop.usehwa.gen.api.process.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Contract {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "패키지 관리 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "pacakgeId")
    private PackageInfo packageInfo;

    @ApiModelProperty(notes = "공정 관리 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "presentProcessId")
    private Process presentProcess;

    @ApiModelProperty(notes = "계약 번호")
    @Column(nullable = false, length = 20, unique = true)
    private String contractNumber;

    @ApiModelProperty(notes = "고객명")
    @Column(nullable = false, length = 20)
    private String customerName;

    @ApiModelProperty(notes = "고객 전화번호")
    @Column(nullable = false, length = 13, unique = true)
    private String customerPhone;

    @ApiModelProperty(notes = "계약일자")
    @Column(nullable = false)
    private LocalDate contractDate;

    @ApiModelProperty(notes = "업무 시작일")
    @Column(nullable = false)
    private LocalDate startWorkDate;

    @ApiModelProperty(notes = "업무 종료일")
    private LocalDate endWorkDate;

}
