package shop.usehwa.gen.api.push.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.push.entity.AppToken;
import shop.usehwa.gen.api.push.entity.Member;
import shop.usehwa.gen.api.push.repository.AppTokenRepository;
import shop.usehwa.gen.common.enums.MemberGroup;
import shop.usehwa.gen.common.exception.CMissingDataException;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AppTokenService {
    private final AppTokenRepository appTokenRepository;

    public List<String> getGroupTokens(MemberGroup memberGroup) {
        List<String> result = new LinkedList<>();

        List<AppToken> tokens = appTokenRepository.findAllByMember_MemberGroup(memberGroup);
        for (AppToken token : tokens) {
            result.add(token.getAppToken());
        }

        return result;
    }

    public List<String> getAllTokens() {
        List<String> result = new LinkedList<>();

        List<AppToken> tokens;
        tokens = appTokenRepository.findAllByMember_IsEnabled(true);

        for (AppToken token : tokens) {
            result.add(token.getAppToken());
        }

        return result;
    }

    public List<String> getSingleToken(Member member) {
        List<String> result = new LinkedList<>();

        AppToken token = appTokenRepository.findByMember(member).orElseThrow(CMissingDataException::new);
        result.add(token.getAppToken());

        return result;
    }

}
