package shop.usehwa.gen.api.push.service;

import shop.usehwa.gen.api.push.interceptors.HeaderRequestInterceptor;
import shop.usehwa.gen.common.exception.CMissingDataException;
import org.springframework.stereotype.Service;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


@Service
public class AndroidPushService {
    @Value("${firebase.api.url}")
    private String FIREBASE_API_URL;

    @Value("${firebase.server.key}")
    private String FIREBASE_SERVER_KEY;

    @Async
    public void callFCM(HttpEntity<String> entity) {
        RestTemplate restTemplate = new RestTemplate();

        ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new HeaderRequestInterceptor("Authorization",  "key=" + FIREBASE_SERVER_KEY));
        interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json; UTF-8 "));
        restTemplate.setInterceptors(interceptors);

        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
        restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);
    }

    public void sendPush(List<String> sendTokens, String titleText, String bodyText) {
        JSONObject body = new JSONObject();

        JSONArray tokens = new JSONArray();
        for (String item : sendTokens) {
            tokens.put(item);
        }
        body.put("registration_ids", tokens);

        JSONObject notification = new JSONObject();
        notification.put("title", titleText);
        notification.put("body", bodyText);
        body.put("notification", notification);

        HttpEntity<String> request = new HttpEntity<>(body.toString());

        try {
            callFCM(request);
        } catch (Exception e) {
            throw new CMissingDataException(); // 푸시알림 전송실패 라고 보내기
        }
    }

}

