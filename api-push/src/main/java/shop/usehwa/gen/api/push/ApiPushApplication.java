package shop.usehwa.gen.api.push;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPushApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiPushApplication.class, args);
    }
}
