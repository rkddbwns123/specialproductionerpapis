package shop.usehwa.gen.api.push.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.push.entity.AppToken;
import shop.usehwa.gen.api.push.entity.Member;
import shop.usehwa.gen.common.enums.MemberGroup;

import java.util.List;
import java.util.Optional;

public interface AppTokenRepository extends JpaRepository<AppToken, Long> {
    List<AppToken> findAllByMember_MemberGroup(MemberGroup memberGroup);
    Optional<AppToken> findByMember(Member member);

    List<AppToken> findAllByMember_IsEnabled(boolean isEnabled);
}
