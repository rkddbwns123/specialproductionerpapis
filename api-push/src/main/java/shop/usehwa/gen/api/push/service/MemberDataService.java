package shop.usehwa.gen.api.push.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.push.entity.Member;
import shop.usehwa.gen.api.push.repository.MemberRepository;
import shop.usehwa.gen.common.exception.CMissingDataException;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;

    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }
}
