package shop.usehwa.gen.api.push.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.push.entity.Member;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
