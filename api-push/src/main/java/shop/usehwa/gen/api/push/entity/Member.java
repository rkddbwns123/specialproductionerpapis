package shop.usehwa.gen.api.push.entity;

import lombok.Getter;
import lombok.Setter;
import shop.usehwa.gen.common.enums.MemberGroup;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private MemberGroup memberGroup;

    @Column(nullable = false, length = 20)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private LocalDateTime joinDate;

    @Column(nullable = false)
    private Boolean isEnabled;
}
