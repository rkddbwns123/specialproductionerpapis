package shop.usehwa.gen.api.push.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.usehwa.gen.api.push.entity.Member;
import shop.usehwa.gen.api.push.model.SendPushRequest;
import shop.usehwa.gen.api.push.service.AndroidPushService;
import shop.usehwa.gen.api.push.service.AppTokenService;
import shop.usehwa.gen.api.push.service.MemberDataService;
import shop.usehwa.gen.common.response.model.CommonResult;
import shop.usehwa.gen.common.response.service.ResponseService;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "앱 푸시 발송 관리")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/app-push")
public class AppPushSendController {
    private final AndroidPushService androidPushService;
    private final AppTokenService appTokenService;
    private final MemberDataService memberDataService;

    @ApiOperation(value = "개별 푸시")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "회원시퀀스", required = true),
    })
    @PostMapping("/send/person/{memberId}")
    public CommonResult sendPerson(@PathVariable long memberId, @RequestBody @Valid SendPushRequest sendPushRequest) {
        Member member = memberDataService.getData(memberId);

        List<String> sendTokens = appTokenService.getSingleToken(member);
        androidPushService.sendPush(sendTokens, "[개인알림] " + sendPushRequest.getPushTitle(), sendPushRequest.getPushBody());

        return ResponseService.getSuccessResult();
    }

}
