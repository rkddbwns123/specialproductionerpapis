package shop.usehwa.gen.api.member.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.member.entity.Member;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {
    private String username;

    private MemberItem(MemberItemBuilder builder) {
        this.username = builder.username;
    }
    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {
        private final String username;

        public MemberItemBuilder(Member member) {
            this.username = member.getUsername();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}