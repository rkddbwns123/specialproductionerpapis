package shop.usehwa.gen.api.member.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import shop.usehwa.gen.api.member.model.MemberRequest;
import shop.usehwa.gen.api.member.model.MemberUpdateRequest;
import shop.usehwa.gen.common.enums.MemberGroup;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;

@Entity
@Getter
@NoArgsConstructor
public class Member implements UserDetails {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "아이디")
    @Column(nullable = false, length = 20, unique = true)
    private String username;

    @ApiModelProperty(notes = "비밀번호")
    @Column(nullable = false)
    private String password;

    @ApiModelProperty(notes = "권한")
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private MemberGroup memberGroup;

    @ApiModelProperty(notes = "팀 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "teamId", nullable = false)
    private Team team;

    @ApiModelProperty(notes = "사용 중 여부")
    @Column(nullable = false)
    private Boolean isUse;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(memberGroup.toString()));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.isUse;
    }

    public void putPassword(String password) {
        this.password = password;
    }

    private Member(MemberBuilder builder) {
        this.username = builder.username;
        this.password = builder.password;
        this.memberGroup = builder.memberGroup;
        this.team = builder.team;
        this.isUse = builder.isUse;
    }

    public void putMember(MemberUpdateRequest request) {
        this.isUse = request.getIsUse();
    }

    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final String username;
        private final String password;
        private final MemberGroup memberGroup;
        private Team team;
        private final Boolean isUse;

        public MemberBuilder(MemberRequest request) {
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.memberGroup = request.getMemberGroup();
            this.isUse = true;
        }

        public MemberBuilder setTeam(Team team) {
            this.team = team;

            return this;
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}