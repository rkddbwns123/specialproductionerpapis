package shop.usehwa.gen.api.member.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.member.entity.Member;
import shop.usehwa.gen.common.enums.MemberGroup;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberDetails {
    private String username;
    private String teamName;
    private String memberGroup;

    private MemberDetails(MemberDetailsBuilder builder) {
        this.username = builder.username;
        this.teamName = builder.teamName;
        this.memberGroup = builder.memberGroup;
    }

    public static class MemberDetailsBuilder implements CommonModelBuilder<MemberDetails> {
        private final String username;
        private String teamName;
        private final String memberGroup;

        public MemberDetailsBuilder(Member member) {
            this.username = member.getUsername();
            this.memberGroup = member.getMemberGroup().getName();

            if(member.getTeam() != null) {
                this.teamName = member.getTeam().getTeamName();
            }
        }
        @Override
        public MemberDetails build() {
            return new MemberDetails(this);
        }
    }
}