package shop.usehwa.gen.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class TeamRequest {
    @ApiModelProperty(value = "팀 명", required = true)
    private String teamName;

    @ApiModelProperty(value = "주 업무", required = true)
    private String mainWork;

    @ApiModelProperty(value = "팀장", required = true)
    private String teamLeader;
}