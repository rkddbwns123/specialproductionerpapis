package shop.usehwa.gen.api.member.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.member.entity.Team;
import shop.usehwa.gen.common.function.ConvertFormat;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TeamItem {

    private String teamName;

    private String mainWork;

    private String teamLeader;

    private String createDate;

    private TeamItem(TeamItemBuilder builder) {
        this.teamName = builder.teamName;
        this.mainWork = builder.mainWork;
        this.teamLeader = builder.teamLeader;
        this.createDate = builder.createDate;
    }

    public static class TeamItemBuilder implements CommonModelBuilder<TeamItem> {
        private final String teamName;
        private final String mainWork;
        private final String teamLeader;
        private final String createDate;

        public TeamItemBuilder(Team team) {
            this.teamName = team.getTeamName();
            this.mainWork = team.getMainWork();
            this.teamLeader = team.getTeamLeader();
            this.createDate = ConvertFormat.convertLocalDateTimeToStringMini(team.getCreateDate());
        }
        @Override
        public TeamItem build() {
            return new TeamItem(this);
        }
    }
}