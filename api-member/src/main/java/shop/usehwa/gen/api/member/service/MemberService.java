package shop.usehwa.gen.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.member.entity.Member;
import shop.usehwa.gen.api.member.entity.Team;
import shop.usehwa.gen.api.member.model.MemberDetails;
import shop.usehwa.gen.api.member.model.MemberRequest;
import shop.usehwa.gen.api.member.model.MemberUpdateRequest;
import shop.usehwa.gen.api.member.repository.MemberRepository;
import shop.usehwa.gen.api.member.repository.TeamRepository;
import shop.usehwa.gen.common.exception.CDontFindMemberInfoException;
import shop.usehwa.gen.common.response.model.ListResult;
import shop.usehwa.gen.common.response.service.ListConvertService;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;
    private final TeamRepository teamRepository;

    public void setMember(MemberRequest request, long teamId) {
        Team team = teamRepository.findById(teamId).orElseThrow();
        Member addData = new Member.MemberBuilder(request).setTeam(team).build();

        memberRepository.save(addData);
    }

    public ListResult<MemberDetails> getMembers() {
        List<Member> originList = memberRepository.findAll();

        if (originList.isEmpty()) throw new CDontFindMemberInfoException();

        List<MemberDetails> result = new LinkedList<>();
        originList.forEach(item -> result.add(new MemberDetails.MemberDetailsBuilder(item).build()));
        return ListConvertService.settingResult(result);
    }

    public void putBoard(long id, MemberUpdateRequest request) {
        Member originData = memberRepository.findById(id).orElseThrow(CDontFindMemberInfoException::new);
        originData.putMember(request);

        memberRepository.save(originData);
    }
}