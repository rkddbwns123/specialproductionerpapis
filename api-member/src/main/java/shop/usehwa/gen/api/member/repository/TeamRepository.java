package shop.usehwa.gen.api.member.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.member.entity.Team;

public interface TeamRepository extends JpaRepository<Team, Long> {

}