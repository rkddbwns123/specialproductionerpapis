package shop.usehwa.gen.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.member.configure.JwtTokenProvider;
import shop.usehwa.gen.api.member.entity.Member;
import shop.usehwa.gen.api.member.model.LoginRequest;
import shop.usehwa.gen.api.member.model.LoginResponse;
import shop.usehwa.gen.api.member.repository.MemberRepository;
import shop.usehwa.gen.common.enums.MemberGroup;
import shop.usehwa.gen.common.exception.CMissingDataException;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    public LoginResponse doLogin(MemberGroup memberGroup, LoginRequest loginRequest, String loginType) {
        Member member = memberRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다. 던지기

        if (!member.getIsUse()) throw new CMissingDataException(); // 회원정보가 없습니다. 던지기
        if (!member.getMemberGroup().equals(memberGroup)) throw new CMissingDataException(); // 회원정보가 없습니다. 던지기
        if (!passwordEncoder.matches(loginRequest.getPassword(), member.getPassword())) throw new CMissingDataException(); // 회원정보가 없습니다. 던지기

        String token = jwtTokenProvider.createToken(member.getId(), member.getMemberGroup().toString(), loginType);

        return new LoginResponse.LoginResponseBuilder(token, member.getUsername()).build();
    }
}