package shop.usehwa.gen.api.member.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.member.model.TeamRequest;
import shop.usehwa.gen.api.member.model.TeamUpdateRequest;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Team {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "팀명")
    @Column(nullable = false, length = 20)
    private String teamName;

    @ApiModelProperty(notes = "주 업무")
    @Column(nullable = false, length = 50)
    private String mainWork;

    @ApiModelProperty(notes = "팀장")
    @Column(nullable = false, length = 20)
    private String teamLeader;

    @ApiModelProperty(notes = "사용 중 여부")
    @Column(nullable = false)
    private Boolean isUse;

    @ApiModelProperty(notes = "등록일자")
    @Column(nullable = false)
    private LocalDateTime createDate;

    private Team(TeamBuilder builder) {
        this.teamName = builder.teamName;
        this.mainWork = builder.mainWork;
        this.teamLeader = builder.teamLeader;
        this.isUse = builder.isUse;
        this.createDate = builder.createDate;
    }

    public void putTeam(TeamUpdateRequest request) {
        this.isUse = request.getIsUse();
    }

    public static class TeamBuilder implements CommonModelBuilder<Team> {
        private final String teamName;
        private final String mainWork;
        private final String teamLeader;
        private final Boolean isUse;
        private final LocalDateTime createDate;

        public TeamBuilder(TeamRequest request) {
            this.teamName = request.getTeamName();
            this.mainWork = request.getMainWork();
            this.teamLeader = request.getTeamLeader();
            this.isUse = true;
            this.createDate = LocalDateTime.now();
        }

        @Override
        public Team build() {
            return new Team(this);
        }
    }
}